async function leerJSON(url) {

  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch(err) {
    
    alert(err);
  }
}

function leerCasos()
{
var url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
//var resultado=document.getElementById("r");
var msg="";

leerJSON(url).then(datos=>{
console.log(datos);
definirDepartamentos(datos);
crearTablaDepartamentos(datos);
})

}

function leerCasos2()
{
var url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
leerJSON(url).then(datos=>{
console.log(datos);
definirMunicipios(datos);
crearTablaMunicipios(datos);
})
}

function leerCasos3()
{
var url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
leerJSON(url).then(datos=>{
console.log(datos);
crearTablaTotal(datos);
})
}

function releerCasos()
{
var url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
leerJSON(url).then(datos=>{
crearTablaDepartamentos(datos);
})
}

function releerCasos2()
{
var url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
leerJSON(url).then(datos=>{
crearTablaMunicipios(datos);
})
}

function eliminarRepetidos(quitar) {

[].slice.call(quitar.options).map(function(a){
	if(this[a.value])
	{
		quitar.removeChild(a);
	} else {
		this[a.value]=1;
	}
},{});

}

function definirDepartamentos(datos)
{
	var select="";
	select = "<select name='departamentos' onchange='releerCasos()' id='depart'>";
	for(let i=0;i<datos.length;i++){

		select+="<option> "+datos[i].departamento_nom+" </option>";
	}
	select+="</select>";
	document.getElementById("red").innerHTML=select;
	var depart = document.getElementById("depart");
	eliminarRepetidos(depart);
}

function definirMunicipios(datos)
{
	var select2="";
	select2 = "<select name='municipios' onchange='releerCasos2()' id='muni'>";
	for(let i=0;i<datos.length;i++){

		select2+="<option> "+datos[i].ciudad_municipio_nom+" </option>";
	}
	select2+="</select>";
	document.getElementById("red2").innerHTML=select2;
	var muni = document.getElementById("muni");
	eliminarRepetidos(muni);
}


function crearTablaDepartamentos(datos)
{
	var cantM=0;
	var cantF=0;
	var data = new google.visualization.DataTable();
	var data2 = new google.visualization.DataTable();
	crearEncabezados(data);
	encabezadoGrafico(data2);

	var cont=0;
	var numRows = document.getElementById("depart").value;
	for(let i=0;i<datos.length;i++)
	{
		if(datos[i].departamento_nom==numRows)
		{
			cont++;
		}
	}
	data.addRows(cont);
	data2.addRows(2);
	cont=0;
	for(let j=0;j<datos.length;j++)
	{
		if(datos[j].departamento_nom==numRows)
		{
			data.setCell(cont,0,datos[j].departamento_nom);
			data.setCell(cont,1,datos[j].departamento);
			data.setCell(cont,2,datos[j].ciudad_municipio_nom);
			data.setCell(cont,3,datos[j].ciudad_municipio);
			data.setCell(cont,4,datos[j].sexo);
			data.setCell(cont,5,datos[j].edad);
			data.setCell(cont,6,datos[j].ubicacion);
			data.setCell(cont,7,datos[j].estado);
			cont++;
				if(datos[j].sexo=='M'){
					cantM++;
				}else{
					cantF++;
				}
		}	
	}
	//data.sort({column: 4, desc: true});
	var table = new google.visualization.Table(document.getElementById('table_div1'));
    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

    crearGrafico(data2,cantM,cantF);
}

function crearGrafico(data2, masculino, femenino)
{
	data2.setCell(0,0,'Masculino');
	data2.setCell(0,1,masculino);
	data2.setCell(1,0,'Femenino');
	data2.setCell(1,1,femenino);

	var options = {
          title: 'Cantidad de casos positivos COIVD-19 por su tipo de sexo:',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data2, options);
      
}

function crearTablaMunicipios(datos)
{
	var data = new google.visualization.DataTable();
	crearEncabezados2(data);

	var cont=0;
	var numRows = document.getElementById("muni").value;
	for(let i=0;i<datos.length;i++)
	{
		if(datos[i].ciudad_municipio_nom==numRows)
		{
			cont++;
		}
	}
	data.addRows(cont);
	cont=0;
	for(let j=0;j<datos.length;j++)
	{
		if(datos[j].ciudad_municipio_nom==numRows)
		{
			data.setCell(cont,0,datos[j].ciudad_municipio_nom);
			data.setCell(cont,1,datos[j].ciudad_municipio);
			data.setCell(cont,2,datos[j].fuente_tipo_contagio);
			data.setCell(cont,3,datos[j].sexo);
			data.setCell(cont,4,datos[j].edad);
			data.setCell(cont,5,datos[j].ubicacion);
			data.setCell(cont,6,datos[j].estado);
			cont++;
		}	
	}
	data.sort({column: 2, desc: true});
	var table = new google.visualization.Table(document.getElementById('table_div2'));
    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
}

function crearTablaTotal(datos)
{
	var data = new google.visualization.DataTable();
	crearEncabezados3(data);
	data.addRows(datos.length);
	for(let j=0;j<datos.length;j++)
	{
			data.setCell(j,0,datos[j].departamento_nom);
			data.setCell(j,1,datos[j].departamento);
			data.setCell(j,2,datos[j].ciudad_municipio_nom);
			data.setCell(j,3,datos[j].ciudad_municipio);
			data.setCell(j,4,datos[j].fuente_tipo_contagio);
			data.setCell(j,5,datos[j].sexo);
			data.setCell(j,6,datos[j].edad);
			data.setCell(j,7,datos[j].ubicacion);
			data.setCell(j,8,datos[j].estado);	
	}
	data.sort({column: 0, desc: true});
	var table = new google.visualization.Table(document.getElementById('table_div3'));
    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
}


function crearEncabezados(data)
{
	data.addColumn('string','Nombre del Departamento');
	data.addColumn('string','Numero Departamento');
	data.addColumn('string','Nombre del Municipio');
	data.addColumn('string','Numero Municipio');
	data.addColumn('string','Sexo').sortColumn;
	data.addColumn('string','Edad');
	data.addColumn('string','Ubicacion');
	data.addColumn('string','Estado');

}

function crearEncabezados2(data)
{
	data.addColumn('string','Nombre del Municipio');
	data.addColumn('string','Numero Municipio');
	data.addColumn('string','Tipo de contagio');
	data.addColumn('string','Sexo');
	data.addColumn('string','Edad');
	data.addColumn('string','Ubicacion');
	data.addColumn('string','Estado');


}

function crearEncabezados3(data)
{
	data.addColumn('string','Nombre del Departamento');
	data.addColumn('string','Numero Departamento');
	data.addColumn('string','Nombre del Municipio');
	data.addColumn('string','Numero Municipio');
	data.addColumn('string','Tipo de contagio');
	data.addColumn('string','Sexo').sortColumn;
	data.addColumn('string','Edad');
	data.addColumn('string','Ubicacion');
	data.addColumn('string','Estado');

}

function encabezadoGrafico(data2)
{
	data2.addColumn('string','Sexo');
	data2.addColumn('number','Cantidad');
}
