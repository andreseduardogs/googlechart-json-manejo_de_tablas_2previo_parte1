![Casoscovid](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### 2Previo-parte1 Lectura de JSON - Visualización de datos usando la API de google chart - Manejo de tablas
***
## Índice
1. [Características](#caracter-sticas-)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)
***

#### Características:

  - Se presenta un webapp, la caul permite ver los casos positivos registrados en tiempos de pandemia por el COVID-19
  - Proyecto con lectura de datos json a través de la API fecth JavaScript
  - proyecto con visualizacion basada en la API de google chart [table chart](https://developers.google.com/chart/interactive/docs/gallery/table)
  - Carga dinámica del JSON, dado por la pagina de [datos abiertos de colombia](https://www.datos.gov.co)
  - Archivo json de ejemplo: [ver](https://www.datos.gov.co/resource/gt2j-8ykr.json)
  - CSS y paleta de colores definida por bootstrap [Descargar Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/download/)
***
  #### Contenido del proyecto
  - [index.html](https://gitlab.com/andreseduardogs/googlechart-json-manejo_de_tablas_2previo_parte1/-/blob/master/index.html): Archivo principal con enlaces hacia ref1, ref2 y ref3 los cuales cargan el JSON
  - [js/estadistica.js](https://gitlab.com/andreseduardogs/googlechart-json-manejo_de_tablas_2previo_parte1/-/blob/master/js/estadistica.js): Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados
  - [html/ref1.html](https://gitlab.com/andreseduardogs/googlechart-json-manejo_de_tablas_2previo_parte1/-/blob/master/html/ref1.html): Archivo html ref1, muestra casos positivos de covid-19 por departamentos
  - [html/ref2.html](https://gitlab.com/andreseduardogs/googlechart-json-manejo_de_tablas_2previo_parte1/-/blob/master/html/ref2.html): Archivo html ref2, muestra casos positivos de covid-19 por municipios
  - [html/ref3.html](https://gitlab.com/andreseduardogs/googlechart-json-manejo_de_tablas_2previo_parte1/-/blob/master/html/ref3.html): Archivo html ref3, muestra el total de casos registrados en colombia almacenados en el JSON

***
#### Tecnologías

  - HTML5
  - JavaScript
  - CSS(definido por Bootstrap)

Usted puede ver el siguiente marco conceptual sobre la API fetch:

  - [Vídeo explicativo lectura con fetch()](https://www.youtube.com/watch?v=DP7Hkr2ss_I)
  - [Gúia de Mozzilla JSON](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON)
  - [API google chart](https://developers.google.com/chart)
  
  ***
#### IDE

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

***
### Instalación

  - Herramienta sublime text 3-> [descargar](https://www.sublimetext.com/3).
  - Firefox Devoloper Edition-> [descargar](https://www.mozilla.org/es-ES/firefox/developer/).
El software es necesario para ver la interacción por consola y depuración del código JS


```sh
-Invocar página index.html desde Firefox 
-Contenido del proyecto:

|   index.html
+---html
|       ref1.html
|       ref2.html
|       ref3.html
|       
\---js
        estadistica.js
```

***
### Demo

 - Para ver el demo de la aplicación puede dirigirse a la pagina: [casoscovid](http://ufps35.madarme.co/casoscovid/).
 - Una vez dentro de la pagina se encuentra una imagen referente a el tema junto con una paqueña descripcion de esta.
 - Se encuentran tres vinculos a los respectivo ref1, ref2, ref3 en los cuales se interactua con los datos del JSON
 - Ref1 muestra los casos registrados positivos de covid-19, seleccionando un departamento en especifico y resaltando en una tabla estilo torta la cantidad de hombre y mujures (por defento BOGOTA)
 - Ref2 muestra los casos registrados positivos de covid-19, seleccionando un municipio en especifico ordenando por el tipo de contagio
 - Ref3 muestra los casos registrados positivos de covid-19, muestra el total de los casos registrados ordenandolos por su respectivo departamento

***
### Autor(es)
  - Proyecto realizado por el estudiante Andres Eduardo Garcia Sosa, representado por el codigo: 1151861(andreseduardogs@ufps.edu.co).



***
### Institución Académica   
Proyecto desarrollado en la Materia programación web(Grupo B) del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
